import discord
import json
import pickle
import os
from PIL import Image
import requests
from datetime import datetime
from io import StringIO
import random
import pyautogui
import time
from threading import Thread

# Writer class
class Writer(Thread):

    #static variable
    enable = False

    # writer constructor for thread
    def __init__(self):
        Thread.__init__(self)

    # basic operation for the bot
    def run(self):
        # while it exists
        while(True):
            # build a random string
            send = ""
            # fill it with random stuff
            for i in range(0, random.randint(1,10)):
                send += chr(random.randint(64,100))
            # send it
            if(self.enable):
                Writer.write(send)
            time.sleep(4)

    # basic write operation from string
    def write(something):
        # translate from azerty to qwerty
        trans = str.maketrans('azertyuiopqsdfghjklmwxcvbn!AZERTYUIOPQSDFGHJKLMWXCVBN.-é:_', 'qwertyuiopasdfghjkl,zxcvbn&QWERTYUIOPASDFGHJKL?ZXCVBN/]eM)')
        # write it because this is fucking usefull
        pyautogui.typewrite(something.translate(trans))
        # send the message
        pyautogui.press("enter")

    # catch operation because we love that
    def catch(pokemon):
        print("write catch command")
        # build the command
        phrase = "p!catch " + pokemon
        # eject useless shit already tiped
        pyautogui.press("enter")
        # tip it and catch this bitch
        Writer.write(phrase)