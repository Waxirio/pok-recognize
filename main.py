import discord
import json
import pickle
import os
from PIL import Image
import requests
from datetime import datetime
from io import StringIO
import random
import pyautogui
import time
from threading import Thread
from Writer import Writer

client = discord.Client()

# bot's prefix
prefix = ""
# size in pixel of the images
imSize = 475*475
scans = 200
middleNbPixel = 112712
# basic sample list
pokeSampleList = pickle.load(open('./data/basic_samples.pck', 'rb'))
pokeNameList = pickle.load(open('./data/basic_names.pck', 'rb'))
# alolan sample list
alolanPokeSampleList = pickle.load(open('./data/alolan_samples.pck', 'rb'))
alolanPokeNameList = pickle.load(open('./data/alolan_names.pck', 'rb'))
# shiny sample list
shinyPokeSampleList = pickle.load(open('./data/shiny_samples.pck', 'rb'))
shinyPokeNameList = pickle.load(open('./data/shiny_names.pck', 'rb'))
# mega sample list
megaPokeSampleList = pickle.load(open('./data/mega_samples.pck', 'rb'))
megaPokeNameList = pickle.load(open('./data/mega_names.pck', 'rb'))
# shiny alolan sample list
shinyAlolanPokeSampleList = pickle.load(open('./data/shiny_alolan_samples.pck', 'rb'))
shinyAlolanPokeNameList = pickle.load(open('./data/shiny_alolan_names.pck', 'rb'))
# shiny mega sample list
shinyMegaPokeSampleList = pickle.load(open('./data/shiny_mega_samples.pck', 'rb'))
shinyMegaPokeNameList = pickle.load(open('./data/shiny_mega_names.pck', 'rb'))
# primal sample list
primalPokeSampleList = pickle.load(open('./data/primal_samples.pck', 'rb'))
primalPokeNameList = pickle.load(open('./data/primal_names.pck', 'rb'))
# pokedex
pokedex = pickle.load(open('./data/pokedex.pck', 'rb'))

def searchParam(srcSample, allSample, allNames):
    print("searching")
    res = None
    found = False
    # search on alolan pokemons
    for pokeSample in allSample:
        if not found:
            # correspondance
            correspondance = 0
            # compare random pixels
            for i in range (0, scans):
                # if pixel are similar
                if srcSample[i] == pokeSample[i]:
                    # correspondance ++
                    correspondance += 1
                else:
                    # pass to the next one
                    i = scans
            # if all scans are success
            if(scans == correspondance):
                # the pokemon may be the right one
                res = allNames[allSample.index(pokeSample,0,len(allSample))]
                found = True
                print("trouvé : " + res)
                # add pokemon to pokedex
                addPokedex(res)
    return res

# Search pokemon name from source image
def search(message, imSource):
    print("recherche ...")
    # create the source sample
    pokeSampleSource = loadImage(imSource)
    print("image loaded!")
    needToRestart = False
    if(Writer.enable):
        needToRestart = True
        Writer.enable = False

    #SHINY ALOLAN
    res = searchParam(pokeSampleSource, shinyAlolanPokeSampleList, shinyAlolanPokeNameList)
    if res != None :
        Writer.catch(res[:-1])
    #SHINY MEGA
    res = searchParam(pokeSampleSource, shinyMegaPokeSampleList, shinyMegaPokeNameList)
    if res != None :
        Writer.catch(res[:-1])
    #ALOLAN
    res = searchParam(pokeSampleSource, alolanPokeSampleList, alolanPokeNameList)
    if res != None :
        Writer.catch(res[:-1])
    #MEGA
    res = searchParam(pokeSampleSource, megaPokeSampleList, pokeNameList)
    if res != None :
        Writer.catch(res[:-1])
    #NORMAL
    res = searchParam(pokeSampleSource, pokeSampleList, pokeNameList)
    if res != None :
        Writer.catch(res[:-1])
    #SHINY
    res = searchParam(pokeSampleSource, shinyPokeSampleList, shinyPokeNameList)
    if res != None :
        Writer.catch(res[:-1])
    #SHINY
    res = searchParam(pokeSampleSource, primalPokeSampleList, primalPokeNameList)
    if res != None :
        Writer.catch(res[:-1])

    if (needToRestart) :
        Writer.enable = True
    print("fin catch")

# Create pokedex for the bot
def addPokedex(pokemon):
    # if it already catch a pokemon
    if (pokemon in pokedex):
        # add one to number of caught poke
        pokedex[pokemon] += 1
    else:
        # create new line
        pokedex[pokemon] = 1
    # save the pokedex for later
    pickle.dump(pokedex, open('./data/pokedex.pck', 'wb'))

# Load sample list from image
def loadImage(im):
    # create empty list
    tmpList = []
    # get data from them
    pix = list(im.getdata())
    #Loop in image
    for i in range (middleNbPixel, middleNbPixel+scans):
        # add pixels
        tmpList.append(pix[i])
    # add list
    return tmpList

# When bot is connected
@client.event
async def on_ready():
    print('We have logged in as {0.user}'.format(client))

# When a message is send
@client.event
async def on_message(message):
    # If it's an embed message
    if message.embeds:
        # If it contains a image
        if message.embeds[0].image.url:
            # get image
            print("image : " + message.embeds[0].image.url)
            # get url from image
            url = message.embeds[0].image.url
            # check the name of the corresponding pokemon
            emb = search(message, Image.open(requests.get(url, stream=True).raw))
        else :
            # if embed message doesn't contain image
            print("embed sans image")
    elif message.content == prefix+"dex":
        for key, value in pokedex.items():
            print(str(key) + "\t\t|\t"+str(value)[:-1])
    elif message.content == prefix+"start":
        print("auto-typer : starting...")
        Writer.enable = True
        print("auto-typer : start done")
    elif message.content == prefix+"stop":
        print("auto-typer : stopping ...")
        Writer.enable = False
        print("auto-typer : stop done")
    else :
        # if message isn't embed
        print("message recu : " + message.content)

if __name__ == "__main__":
    # load images sample
    print("Images fully loaded : done")
    # load bot's config
    threadWriter = Writer()
    threadWriter.start()
    print("Thread : created")
    with open('./data/config.json') as json_file:
        # Read Json file
        data = json.load(json_file)
        # Load config data
        prefix = data["prefix"]
        # Launch this stuff
        client.run(data["token"])
    threadWriter.join()

    
    